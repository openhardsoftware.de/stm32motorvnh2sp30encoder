//
//--------------------------------
//  Library MotorEncoder
//--------------------------------
//
#include "MotorEncoder.h"
//
extern CError SCPError;
extern CMotorEncoder EncoderSlitPX;
extern CMotorEncoder EncoderSlitDX;
extern CSerial SerialCommand;
//
CMotorEncoder::CMotorEncoder(int pinpulsea, int pinpulseb,
                             TInterruptFunction pirqfunctionpulsea, 
                             TInterruptFunction pirqfunctionpulseb)
{
  FState = INIT_STATEMOTORENCODER;
  FPIrqFunctionPulseA = pirqfunctionpulsea;
  FPIrqFunctionPulseB = pirqfunctionpulseb;
  FPinPulseA = pinpulsea;
  FPinPulseB = pinpulseb;
  FStepCountActual = INIT_PULSECOUNT;
  FStepCountMarker = INIT_PULSECOUNT;
}

bool CMotorEncoder::Open()
{
  pinMode(FPinPulseA, INPUT_PULLDOWN);
  attachInterrupt(FPinPulseA, FPIrqFunctionPulseA, CHANGE);
  pinMode(FPinPulseB, INPUT_PULLDOWN);
  attachInterrupt(FPinPulseB, FPIrqFunctionPulseB, CHANGE);
  return true;
}

bool CMotorEncoder::Close()
{
  pinMode(FPinPulseA, INPUT_PULLDOWN);
  detachInterrupt(FPinPulseA);
  pinMode(FPinPulseB, INPUT_PULLDOWN);
  detachInterrupt(FPinPulseB);
  return true;
}


////                                0b0000.0010        0b0000.0001
//void CMotorEncoder::HandleLevel(bool levelactualb, bool levelactuala)
//{ //---------- State 0--0
//  //   - B1 A0 - <- B0 A0 -> + B0 A1 +
//  if (!FLevelPresetB && !FLevelPresetA)
//  { //   B0 A0 -> + B0 A1 +
//    if (!levelactualb && levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B1 A0 - <- B0 A0
//    if (levelactualb && !levelactuala)
//    {
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 0--1
//  //   - B0 A0 - <- B0 A1 -> + B1 A1 +
//  if (!FLevelPresetB && FLevelPresetA)
//  { //   B0 A1 -> + B1 A1 +
//    if (levelactualb && levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B0 A0 - <- B0 A1
//    if (!levelactualb && !levelactuala)
//    {
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 1--1
//  //   - B0 A1 - <- B1 A1 -> + B1 A0 +
//  if (FLevelPresetB && FLevelPresetA)
//  { //   B1 A1 -> + B1 A0 +
//    if (levelactualb && !levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B0 A1 - <- B1 A1
//    if (!levelactualb && levelactuala)
//    { 
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//  else //----- State 1--0
//  //   - B1 A1 - <- B1 A0 -> + B0 A0 +
//  if (FLevelPresetB && !FLevelPresetA)
//  { //   B1 A0 -> + B0 A0 +
//    if (!levelactualb && !levelactuala)
//    { 
//      FStepCountActual++;
//      FState = smePositive;
//    }
//    else
//    // - B1 A1 - <- B1 A0
//    if (levelactualb && levelactuala)
//    { 
//      FStepCountActual--;
//      FState = smeNegative;
//    }
//    else
//    { // error
//      FState = smeError;
//      SCPError.SetCode(ecDecoderStateImpossible);
//    }
//  }
//}

//-------------------------
//  Counterwise <- ++
//-------------------------
//  A-0011001100
//  B-0001100110
//-------------------------
//  AB  Time
//  00  n+0
//  10  n+1
//  11  n+2
//  01  n+3
//  00  n+4
//
//-------------------------
//  CounterClockwise -> --
//-------------------------
//  A-0001100110
//  B-0011001100
//-------------------------
//  AB  Time
//  00  n+0
//  01  n+1
//  11  n+2
//  10  n+3
//  00  n+4
//
void CMotorEncoder::HandleLevel(bool levelactuala, bool levelactualb)
{ //---------- Actual AB[00]
  if (!levelactuala && !levelactualb)
  { // [00] <- [XX]
    if (!FLevelPresetA && !FLevelPresetB)
    { // [00] <- [00] !Error
      FErrorCount++;
    }
    else
    if (!FLevelPresetA && FLevelPresetB)
    { // [00] <- [01] 
      FStepCountActual++;
    }
    else
    if (FLevelPresetA && !FLevelPresetB)
    { // [00] <- [10]
      FStepCountActual--;
    }
    else
    if (FLevelPresetA && FLevelPresetB)
    { // [00] <- [11] !Error
      FErrorCount++;
    }
  }
  else //---------- Actual AB[01]
  if (!levelactuala && levelactualb)
  { // [01] <- [XX]
    if (!FLevelPresetA && !FLevelPresetB)
    { // [01] <- [00]
      FStepCountActual--;
    }
    else
    if (!FLevelPresetA && FLevelPresetB)
    { // [01] <- [01] !Error
      FErrorCount++;
    }
    else
    if (FLevelPresetA && !FLevelPresetB)
    { // [01] <- [10] !Error
      FErrorCount++;
    }
    else
    if (FLevelPresetA && FLevelPresetB)
    { // [01] <- [11]
      FStepCountActual++;
    }    
  }
  else //---------- Actual AB[10]
  if (levelactuala && !levelactualb)
  { // [10] <- [XX]
    if (!FLevelPresetA && !FLevelPresetB)
    { // [10] <- [00]
      FStepCountActual++;
    }
    else
    if (!FLevelPresetA && FLevelPresetB)
    { // [10] <- [01] !Error
      FErrorCount++;
    }
    else
    if (FLevelPresetA && !FLevelPresetB)
    { // [10] <- [10] !Error      
      FErrorCount++;
    }
    else
    if (FLevelPresetA && FLevelPresetB)
    { // [10] <- [11]
      FStepCountActual--;
    }    
  }
  else //---------- Actual AB[11]
  if (levelactuala && levelactualb)
  { // [11] <- [XX]
    if (!FLevelPresetA && !FLevelPresetB)
    { // [11] <- [00] !Error
      FErrorCount++;
    }
    else
    if (!FLevelPresetA && FLevelPresetB)
    { // [11] <- [01]
      FStepCountActual--;
    }
    else
    if (FLevelPresetA && !FLevelPresetB)
    { // [11] <- [10]
      FStepCountActual++;
    }
    else
    if (FLevelPresetA && FLevelPresetB)
    { // [11] <- [11] !Error
      FErrorCount++;
    }        
  }
}

void CMotorEncoder::HandleInterrupt()
{
  bool LevelActualA = (0 < digitalRead(FPinPulseA));
  bool LevelActualB = (0 < digitalRead(FPinPulseB));
  HandleLevel(LevelActualA, LevelActualB);
  FLevelPresetA = LevelActualA;
  FLevelPresetB = LevelActualB;
}

//???
//void CMotorEncoder::HandleSerial(CSerial& serial)
//{
//  if (FStepCountMarker != FStepCountActual)
//  {
//    serial.Write("PCA[");
//    serial.Write((Int32)FStepCountActual);
//    serial.Write("] PCD[");
//    serial.Write((Int32)(FStepCountActual - FStepCountMarker));
//    serial.WriteLine("]");
//    //
//    FStepCountMarker = FStepCountActual;
//  }  
//}
  
