//
//--------------------------------
//  Library Key
//--------------------------------
//
#include "Key.h"
//
CKey::CKey(int pin)
{
  FPin = pin;
  FInverted = false;
}

CKey::CKey(int pin, bool inverted)
{
  FPin = pin;
  FInverted = inverted;
}

Boolean CKey::Open()
{
  pinMode(FPin, INPUT);
  return true;
}

Boolean CKey::Close()
{
  pinMode(FPin, INPUT);
  return true;
}
