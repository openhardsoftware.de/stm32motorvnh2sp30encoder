//
//--------------------------------
//  Library WatchDog
//--------------------------------
//
#ifndef WatchDog_h
#define WatchDog_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
#include "IOPin.h"
//
//--------------------------------
//  Section - WatchDog
//--------------------------------
//
class CWatchDog : public CIOPin
{
  private:
  UInt32 FCounterPreset, FPresetHigh, FCounterActual;
  //  
  public:
  CWatchDog(int pin);
  //
  Boolean Open(UInt32 counterpreset, UInt32 presethigh);
  Boolean Close();
  //
  void SetOn();
  void SetOff();
  void Trigger();
  void ForceTrigger();
};
//
#endif // WatchDog_h
