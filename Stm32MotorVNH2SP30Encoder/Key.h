//
//--------------------------------
//  Library Key
//--------------------------------
//
#ifndef Key_h
#define Key_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
class CKey
{
  private:
  int FPin;
  Boolean FInverted;
  //  
  public:
  CKey(int pin);
  CKey(int pin, bool inverted);
  //
  inline Boolean IsInverted()
  {
    return FInverted;
  }
  inline Boolean IsPressed(void)
  {
    Boolean Level = (HIGH == digitalRead(FPin));
    if (FInverted)
    {
      return !Level;
    }
    return Level;
  }  
  //
  Boolean Open();
  Boolean Close();
};
//
#endif // Key
