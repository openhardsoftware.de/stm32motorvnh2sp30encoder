//
//--------------------------------
//  Library MotorDriverVNH2SP30
//--------------------------------
//
#include "MECCommand.h"
#include "MotorDriverVNH2SP30.h"
//
extern CSerial SerialCommand;
extern CCommand MECCommand;
//
//--------------------------------
//  Segment - Constructor
//--------------------------------
//
CMotorDriverVNH2SP30::CMotorDriverVNH2SP30(int pinina, int pininb, 
                                           int pinena, int pinenb, 
                                           int pinpwm, int pincs)
{
  FPinPWM = pinpwm;
  FPinINA = pinina;
  FPinINB = pininb;
  FPinENA = pinena;
  FPinENB = pinenb;
  FPinCS = pincs;
  FState = smdUndefined;
  FVelocityPercent = ZERO_VELOCITYPERCENT;
}
//
//--------------------------------
//  Segment - Property
//--------------------------------
//
EStateMotorDriverVNH2SP30 CMotorDriverVNH2SP30::GetState()
{
  return FState;
}
void CMotorDriverVNH2SP30::SetState(EStateMotorDriverVNH2SP30 state)
{
  if (state != FState)
  {
    FState = state;
    // debug !!!!!!!!!!!!!!!! SCPCommand.RiseEvent(SerialCommand, "SSS");
  }
}
//
//--------------------------------
//  Segment - Helper
//--------------------------------
//
Byte CMotorDriverVNH2SP30::VelocityPercentToPwm(int velocitypercent)
{
  Byte PWMValue = (int)(PWM_MINIMUM + (PWM_MAXIMUM - PWM_MINIMUM) * FVelocityPercent / 100);
  if (PWMValue < PWM_MINIMUM)
  {
    PWMValue = PWM_MINIMUM;
  }
  else
  if (PWM_MAXIMUM < PWMValue)
  {
    PWMValue = PWM_MAXIMUM;
  }
  return PWMValue;
}

int CMotorDriverVNH2SP30::PwmToVelocityPercent(Byte pwm)
{
  
}
//
//--------------------------------
//  Segment - Management
//--------------------------------
//
Boolean CMotorDriverVNH2SP30::Open()
{
  pinMode(FPinINA, OUTPUT);
  digitalWrite(FPinINA, LOW);
  pinMode(FPinINB, OUTPUT);
  digitalWrite(FPinINB, LOW);
  pinMode(FPinPWM, PWM);
  digitalWrite(FPinPWM, LOW);
  //
  FState = smdIdle;
  FVelocityPercent = ZERO_VELOCITYPERCENT;
  return true;
}

Boolean CMotorDriverVNH2SP30::Close()
{
  pinMode(FPinINA, INPUT_PULLDOWN);
  pinMode(FPinINB, INPUT_PULLDOWN);
  pinMode(FPinPWM, INPUT_PULLDOWN);
  //
  FState = smdIdle;
  FVelocityPercent = ZERO_VELOCITYPERCENT;
  return true;
}
//
int CMotorDriverVNH2SP30::GetVelocityPercent(void) // [%]
{
  return FVelocityPercent;
}
void CMotorDriverVNH2SP30::SetVelocityPercent(int velocitypercent) // [%]
{
  FVelocityPercent = velocitypercent;
}

Int32 CMotorDriverVNH2SP30::GetPositionTarget()
{
  return FPositionTarget;
}

Int32 CMotorDriverVNH2SP30::GetPositionActual()
{
  return FPositionActual;
}

void CMotorDriverVNH2SP30::SetPositionActual(Int32 positionactual)
{
  FPositionActual = positionactual;
}
//
void CMotorDriverVNH2SP30::StartPositive()
{
  analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
  digitalWrite(FPinINA, HIGH);
  digitalWrite(FPinINB, LOW);
  analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
  SetState(smdPositive);
}

void CMotorDriverVNH2SP30::StartNegative()
{
  analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
  digitalWrite(FPinINA, LOW);
  digitalWrite(FPinINB, HIGH);
  analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
  SetState(smdNegative);
}

void CMotorDriverVNH2SP30::MoveAbsolute(Int32 positionabsolute)
{
  FPositionTarget = positionabsolute;
  if (FPositionActual < FPositionTarget)
  {
    analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
    digitalWrite(FPinINA, HIGH);
    digitalWrite(FPinINB, LOW);
    analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
    SetState(smdPositive);
  }
  else
  if (FPositionActual < FPositionTarget)
  {    
    analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
    digitalWrite(FPinINA, LOW);
    digitalWrite(FPinINB, HIGH);
    analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
    SetState(smdNegative);
  }
  else
  { // ==
    SetState(smdIdle);
  }  
}

void CMotorDriverVNH2SP30::MoveRelative(Int32 positionrelative)
{
  FPositionTarget += positionrelative;
  if (FPositionActual < FPositionTarget)
  {
    analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
    digitalWrite(FPinINA, HIGH);
    digitalWrite(FPinINB, LOW);
    analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
    SetState(smdPositive);
  }
  else
  if (FPositionActual < FPositionTarget)
  {    
    analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
    digitalWrite(FPinINA, LOW);
    digitalWrite(FPinINB, HIGH);
    analogWrite(FPinPWM, VelocityPercentToPwm(FVelocityPercent));
    SetState(smdNegative);
  }
  else
  { // ==
    SetState(smdIdle);
  }    
}

void CMotorDriverVNH2SP30::StopMotion()
{
  analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
  digitalWrite(FPinINA, LOW);
  digitalWrite(FPinINB, LOW);
  SetState(smdIdle);
}

void CMotorDriverVNH2SP30::BreakMotion()
{
  analogWrite(FPinPWM, ZERO_VELOCITYPERCENT);
  digitalWrite(FPinINA, LOW);
  digitalWrite(FPinINB, LOW);
  SetState(smdIdle);
}
