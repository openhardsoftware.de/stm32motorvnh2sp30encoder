//
//--------------------------------
//  Library Led
//--------------------------------
//
#ifndef Led_h
#define Led_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EStateLed
{
  slUndefined = -1,
  slOff = 0,
  slOn = 1
};
//
class CLed
{
  private:
  int FPin;
  Boolean FInverted;
  EStateLed FState;
  UInt32 FPulsePeriod;
  UInt32 FPulseCountPreset;
  UInt32 FPulseCountActual;
  
  public:
  CLed(int pin);
  CLed(int pin, bool inverted);
  //
  EStateLed GetState();
  inline Boolean IsInverted()
  {
    return FInverted;
  }
  UInt32 GetPulsePeriod();
  inline UInt32 GetPulsePeriodHalf()
  {
    return FPulsePeriod / 2;  
  }
  UInt32 GetPulseCountPreset();
  UInt32 GetPulseCountActual();
  inline Boolean IsPulseCountReached()
  {
    return (FPulseCountPreset <= FPulseCountActual);
  } 
  //
  Boolean Open();
  Boolean Close();
  void SetOn();
  void SetOff();
  void SetPulsePeriodCount(UInt32 pulseperiod, UInt32 pulsecount);
};
//
#endif
