//
//--------------------------------
//  Library Error
//--------------------------------
//
#ifndef MECError_h
#define MECError_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "Serial.h"
//
//--------------------------------
//  Section - Definition
//--------------------------------
//
enum EErrorCode
{
  ecNone =                    (int)0,  
  ecUnknown =                 (int)1,
  ecInvalidCommand =          (int)2,
  ecCommandTimingFailure =    (int)3,
  ecCommandInvalidParameter = (int)4,
  ecToManyParameters =        (int)5,
  ecMissingTargetParameter =  (int)6,
  ecFailMountSDCard =         (int)7,
  ecFailOpenCommandFile =     (int)8,
  ecFailWriteCommandFile =    (int)9,
  ecFailCloseCommandFile =    (int)10,
  ecFailUnmountSDCard =       (int)11,
  ecCommandFileNotOpened =    (int)12,
  ecDecoderStateImpossible =  (int)13  
};
//
class CError
{
  private:
  enum EErrorCode FErrorCode;  
  //
  public:
  CError();
  //
  EErrorCode GetCode();
  void SetCode(EErrorCode errorcode);
  //
  Boolean Open();
  Boolean Close();
  //
  Boolean Handle(CSerial &serial);
};
//
#endif // MECError_h
