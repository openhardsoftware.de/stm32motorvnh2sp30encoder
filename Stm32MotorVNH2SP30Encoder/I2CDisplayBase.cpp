#include "I2CDisplayBase.h"
//
// When the display powers up, it is configured as follows:
//
// 1. Display clear
// 2. Function set:
//    DL = 1; 8-bit interface data
//    N = 0; 1-line display
//    F = 0; 5x8 dot character font
// 3. Display on/off control:
//    D = 0; Display off
//    C = 0; Cursor off
//    B = 0; Blinking off
// 4. Entry mode set:
//    I/D = 1; Increment by 1
//    S = 0; No shift
//
// Note, however, that resetting the Arduino doesn't reset the LCD, so we
// can't assume that its in that state when a sketch starts (and the
// LiquidCrystal constructor is called).

CI2CDisplayBase::CI2CDisplayBase(UInt8 i2caddress, UInt8 colcount, UInt8 rowcount, UInt8 charactersize)
{
	FI2CAddress = i2caddress;
	FColCount = colcount;
	FRowCount = rowcount;
	FCharacterSize = charactersize;
	FBacklightValue = LCD_BACKLIGHT;
}

void CI2CDisplayBase::Open() 
{
	Wire.begin();
	FDisplayFunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
	if (1 < FRowCount) 
	{
		FDisplayFunction |= LCD_2LINE;
	}
	// for some 1 line displays you can select a 10 pixel high font
	if ((0 != FCharacterSize) && (1 == FRowCount)) 
	{
		FDisplayFunction |= LCD_5x10DOTS;
	}
	// SEE PAGE 45/46 FOR INITIALIZATION SPECIFICATION!
	// according to datasheet, we need at least 40ms after power rises above 2.7V
	// before sending Commands. Arduino can turn on way befer 4.5V so we'll wait 50
	delay(50);
	// Now we pull both RS and R/W low to begin Commands
	ExpanderWrite(FBacklightValue);	// reset expanderand turn backlight off (Bit 8 =1)
	delay(1000);
	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46
	// we start in 8bit mode, try to set 4 bit mode
	Write4Bits(0x03 << 4);
	delayMicroseconds(4500); // wait min 4.1ms
	// second try
	Write4Bits(0x03 << 4);
	delayMicroseconds(4500); // wait min 4.1ms
	// third go!
	Write4Bits(0x03 << 4);
	delayMicroseconds(150);
	// finally, set to 4-bit interface
	Write4Bits(0x02 << 4);
	// set # lines, font size, etc.
	Command(LCD_FUNCTIONSET | FDisplayFunction);
	// turn the display on with no cursor or blinking default
	FDisplayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	ShowDisplay();
	// clear it off
	Clear();
	// Initialize to default text direction (for roman languages)
	FDisplayMode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	// set the entry mode
	Command(LCD_ENTRYMODESET | FDisplayMode);
	Home();
}

/********** high level Commands, for the user! */
void CI2CDisplayBase::Clear()
{
	Command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	delayMicroseconds(2000);  // this Command takes a long time!
}

void CI2CDisplayBase::Home()
{
	Command(LCD_RETURNHOME);  // set cursor position to zero
	delayMicroseconds(2000);  // this Command takes a long time!
}

void CI2CDisplayBase::SetCursorPosition(UInt8 col, UInt8 row)
{
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if (row > FRowCount) 
	{ // we count rows starting w/0
		row = FRowCount-1;    
	}
	Command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void CI2CDisplayBase::HideDisplay() 
{
	FDisplayControl &= ~LCD_DISPLAYON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}
void CI2CDisplayBase::ShowDisplay() 
{
	FDisplayControl |= LCD_DISPLAYON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}

// Turns the underline cursor on/off
void CI2CDisplayBase::HideCursor() 
{
	FDisplayControl &= ~LCD_CURSORON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}
void CI2CDisplayBase::ShowCursor() 
{
	FDisplayControl |= LCD_CURSORON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}

// Turn on and off the blinking cursor
void CI2CDisplayBase::DisableCursorBlinking()
{
	FDisplayControl &= ~LCD_BLINKON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}
void CI2CDisplayBase::EnableCursorBlinking() 
{
	FDisplayControl |= LCD_BLINKON;
	Command(LCD_DISPLAYCONTROL | FDisplayControl);
}

// These Commands scroll the display without changing the RAM
void CI2CDisplayBase::ScrollDisplayLeft(void)
{
	Command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void CI2CDisplayBase::ScrollDisplayRight(void) 
{
	Command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void CI2CDisplayBase::LeftToRight(void) 
{
	FDisplayMode |= LCD_ENTRYLEFT;
	Command(LCD_ENTRYMODESET | FDisplayMode);
}

// This is for text that flows Right to Left
void CI2CDisplayBase::RightToLeft(void) 
{
	FDisplayMode &= ~LCD_ENTRYLEFT;
	Command(LCD_ENTRYMODESET | FDisplayMode);
}

// This will 'right justify' text from the cursor
void CI2CDisplayBase::EnableAutoScrolling(void) 
{
	FDisplayMode |= LCD_ENTRYSHIFTINCREMENT;
	Command(LCD_ENTRYMODESET | FDisplayMode);
}

// This will 'left justify' text from the cursor
void CI2CDisplayBase::DisableAutoScrolling(void) 
{
	FDisplayMode &= ~LCD_ENTRYSHIFTINCREMENT;
	Command(LCD_ENTRYMODESET | FDisplayMode);
}

// Allows us to fill the first 8 CGRAM locations
// with custom characters
void CI2CDisplayBase::CreateCharacter(UInt8 location, UInt8 charmap[]) 
{
	location &= 0x7; // we only have 8 locations 0-7
	Command(LCD_SETCGRAMADDR | (location << 3));
	for (int i = 0; i < 8; i++) 
	{
		write(charmap[i]);
	}
}

// Turn the (optional) backlight off/on
void CI2CDisplayBase::SetBacklightOff(void) 
{
	FBacklightValue = LCD_NOBACKLIGHT;
	ExpanderWrite(0);
}

void CI2CDisplayBase::SetBacklightOn(void) 
{
	FBacklightValue=LCD_BACKLIGHT;
	ExpanderWrite(0);
}
bool CI2CDisplayBase::GetBacklight() 
{
  return FBacklightValue == LCD_BACKLIGHT;
}


/*********** mid level Commands, for sending data/cmds */

inline void CI2CDisplayBase::Command(UInt8 value) 
{
	Send(value, 0);
}

inline size_t CI2CDisplayBase::write(UInt8 value) 
{
	Send(value, Rs);
	return 1;
}


/************ low level data pushing Commands **********/

// write either Command or data
void CI2CDisplayBase::Send(UInt8 value, UInt8 mode) 
{
	UInt8 highnib=value&0xf0;
	UInt8 lownib=(value<<4)&0xf0;
	Write4Bits((highnib)|mode);
	Write4Bits((lownib)|mode);
}

void CI2CDisplayBase::Write4Bits(UInt8 value) 
{
	ExpanderWrite(value);
	PulseEnable(value);
}

void CI2CDisplayBase::ExpanderWrite(UInt8 _data)
{
	Wire.beginTransmission(FI2CAddress);
	Wire.write((int)(_data) | FBacklightValue);
	Wire.endTransmission();
}

void CI2CDisplayBase::PulseEnable(UInt8 _data)
{
	ExpanderWrite(_data | En);	// En high
	delayMicroseconds(1);		// enable pulse must be >450ns
	ExpanderWrite(_data & ~En);	// En low
	delayMicroseconds(50);		// Commands need > 37us to settle
}

void CI2CDisplayBase::LoadCustomCharacter(UInt8 char_num, UInt8 *rows)
{
	CreateCharacter(char_num, rows);
}

void CI2CDisplayBase::SetBacklight(UInt8 new_val)
{
	if (new_val) 
	{
		SetBacklightOn();		
	} 
	else 
	{
		SetBacklightOff();		
	}
}

void CI2CDisplayBase::PrintString(const char c[])
{ //This function is not identical to the function used for "real" I2C displays
	//it's here so the user sketch doesn't have to be changed
	print(c);
}
