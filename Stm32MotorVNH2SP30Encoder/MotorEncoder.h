//
//--------------------------------
//  Library MotorEncoder
//--------------------------------
//
#ifndef MotorEncoder_h
#define MotorEncoder_h
//
#include "MECDefines.h"
#include "MECPinout.h"
#include "MECError.h"
//
const Byte POSITION_ZERO = 0x00000000;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
//
enum EStateMotorEncoder
{
  smeError      = -2,
  smeUndefined  = -1,
  smeIdle       = 0,
  smePositive   = 1,
  smeNegative   = 2
};
//
const EStateMotorEncoder INIT_STATEMOTORENCODER = smeUndefined;
const long signed INIT_PULSECOUNT = 0;
//
class CMotorEncoder
{
  private:
  EStateMotorEncoder FState;
  int FPinPulseA, FPinPulseB;
  TInterruptFunction FPIrqFunctionPulseA;
  TInterruptFunction FPIrqFunctionPulseB;
  bool FLevelPresetA, FLevelPresetB;
  long signed FStepCountActual;
  long signed FStepCountMarker;
  long unsigned FErrorCount;
  //
  public:
  CMotorEncoder(int pinpulsea, int pinpulseb,
                TInterruptFunction pirqfunctionpulsea, 
                TInterruptFunction pirqfunctionpulseb);
  //
  long signed GetStepCount()
  {
    return FStepCountActual;
  }
  void SetStepCount(long signed data)
  {
    FStepCountActual = data;
  }
  //
  inline long unsigned GetErrorCount(void)
  {
    long unsigned EC = FErrorCount;
    FErrorCount = 0;
    return EC;
  }
  //
  bool Open(void);
  bool Close(void);
  //
  void HandleLevel(bool levelactuala, bool levelactualb);
  void HandleInterrupt();
//???  void HandleSerial(CSerial& serial);
};
//
#endif // MotorEncoder
