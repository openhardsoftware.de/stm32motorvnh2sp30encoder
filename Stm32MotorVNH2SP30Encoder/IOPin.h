//
//--------------------------------
//  Library IOPin
//--------------------------------
//
#ifndef IOPin_h
#define IOPin_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
enum EIOPinDirection
{
  iopdInput = 0,
  iopdOutput = 1
};
enum EIOPinLevel
{
  ioplUndefined = -1,
  ioplOff = 0,
  ioplOn = 1
};
//
class CIOPin
{
  private:
  int FPin;
  bool FInverted;
  EIOPinDirection FDirection;
  EIOPinLevel FLevel;
  //  
  public:
  CIOPin(int pin, EIOPinDirection pindirection);
  CIOPin(int pin, EIOPinDirection pindirection, bool inverted);
  //
  Boolean Open();
  Boolean Close();
  //
  void SetOn();       // Output
  void SetOff();      // Output
  EIOPinLevel GetLevel(); // Input
  void SetInput();
  void SetOutput();
};
//
#endif // IOPin_h
