//
//--------------------------------
//  Library Automation
//--------------------------------
//
#include "Led.h"
#include "MECAutomation.h"
#include "MECProcess.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#include "JoystickAnalog.h"
#include "MotorDriverVNH2SP30.h"
//
extern CProcess SCPProcess;
extern CLed LedSystem;
//
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
//
extern CJoystickAnalog JoystickSlitPXDX;
// extern CJoystickAnalog JoystickSlitPYDY;
//
extern CMotorDriverVNH2SP30 MotorSlitPX;
//extern CMotorDriverVNH2SP30 MotorSlitDX;
//
CAutomation::CAutomation()
{
  FState = saUndefined;
}

EStateAutomation CAutomation::GetState()
{
  return FState;
}
void CAutomation::SetState(EStateAutomation state)
{
  FState = state;
}

Boolean CAutomation::Open()
{
  FState = saIdle;
  return true;
}

Boolean CAutomation::Close()
{  
  FState = saUndefined;
  return true;
}

void CAutomation::HandleUndefined(CSerial &serial)
{ 
  delay(100);  
}

void CAutomation::WriteEvent(CSerial &serial, String text)
{
  serial.WriteLine(text.c_str());
  serial.WritePrompt();
}

void CAutomation::WriteEvent(CSerial &serial, String mask, int value)
{
  serial.Write(mask.c_str(), value);
  serial.Write("\r\n");
  serial.WritePrompt();
}

// Analyse ProgramLock, Keys and Keyboard
void CAutomation::HandleIdle(CSerial &serial)
{ // JoystickAnalog
//  char Line[48];
//  // debug  sprintf(Line, "\r\nAH[%i] AV[%i] SH[%li] SV[%li]", JoystickSlitPXDX.ReadAdcValueH(), JoystickSlitPXDX.ReadAdcValueV(), JoystickSlitPXDX.ReadStickValueH(), JoystickSlitPXDX.ReadStickValueV());
//  // serial.Write(Line);
  Int32 SH = JoystickSlitPXDX.ReadStickValueH();
  if (SH < -16)
  {
    //sprintf(Line, "\r\n<<< SH[%li]", SH);
    //serial.Write(Line);
    //delay(300);
    MotorSlitPX.SetVelocityPercent(-SH);
    MotorSlitPX.StartNegative();
  }
  else
  if (16 < SH)
  {
    //sprintf(Line, "\r\n>>> SH[%li]", SH);
    //serial.Write(Line);
    //delay(300);
    MotorSlitPX.SetVelocityPercent(+SH);
    MotorSlitPX.StartPositive();
  }
  else
  {
    MotorSlitPX.SetVelocityPercent(0);
    MotorSlitPX.StopMotion();    
  }
//  Int32 SV = JoystickSlitPXDX.ReadStickValueV();
//  if (SV < -16)
//  {
//    MotorSlitDX.SetVelocityPercent(-SV);
//    MotorSlitDX.StartNegative();
//  }
//  else
//  if (16 < SV)
//  {
//    MotorSlitDX.SetVelocityPercent(+SV);
//    MotorSlitDX.StartPositive();
//  }
//  else
//  {
// //   MotorSlitDX.SetVelocityPercent(0);
//    MotorSlitDX.StopMotion();    
//  }
}

void CAutomation::HandleReset(CSerial &serial)
{
  delay(100);
  SetState(saIdle);
}


void CAutomation::Handle(CSerial &serial)
{
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Trigger();    
#endif 
  switch (GetState())
  { // Common
    case saIdle:
      HandleIdle(serial);
      break;
    case saReset:
      HandleReset(serial);
      break;
    default: // saUndefined
      HandleUndefined(serial);
      break;
  }  
}
