//
//--------------------------------
//  Library Automation
//--------------------------------
//
#ifndef Automation_h
#define Automation_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
#include "Serial.h"
#include "JoystickAnalog.h"
#include "MotorDriverVNH2SP30.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
#define MASK_EVENTAUTOMATION "%s EVA %s"
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateAutomation
{
  saUndefined = -1,
  saIdle = 0,
  saReset = 1
};
//
class CAutomation
{
  private:
  // Field
  EStateAutomation FState;
  //  
  void WriteEvent(CSerial &serial, String text);
  void WriteEvent(CSerial &serial, String mask, int value);
  //
  public:
  // Constructor
  CAutomation();
  // Property
  EStateAutomation GetState();
  void SetState(EStateAutomation state);
  //
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // State
  void HandleUndefined(CSerial &serial);  
  void HandleIdle(CSerial &serial);  
  void HandleReset(CSerial &serial);  
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif
