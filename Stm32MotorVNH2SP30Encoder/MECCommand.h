#ifndef MECCommand_h
#define MECCommand_h
//
#include "MECDefines.h"
#include "MECProcess.h"
#include "MECAutomation.h"
#include "Utilities.h"
#include "Serial.h"
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
//
#define ARGUMENT_PROJECT    "MVE - MotorVNH2SP30Encoder"
#define ARGUMENT_SOFTWARE   "01V05"
#define ARGUMENT_DATE       "190902"
#define ARGUMENT_TIME       "1825"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_NANOR3
#define ARGUMENT_HARDWARE "NanoR3"
#endif
#ifdef PROCESSOR_UNOR3
#define ARGUMENT_HARDWARE "UnoR3"
#endif
#ifdef PROCESSOR_MEGA2560
#define ARGUMENT_HARDWARE "Mega2560"
#endif
#ifdef PROCESSOR_DUEM3
#define ARGUMENT_HARDWARE "DueM3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_GPC   "GPC"
#define SHORT_SPC   "SPC"
#define SHORT_GPP   "GPP"
#define SHORT_SPP   "SPP"
#define SHORT_GPW   "GPW"
#define SHORT_SPW   "SPW"
#endif
#define SHORT_RSS   "RSS"
#if defined(WATCHDOG_ISPLUGGED)
#define SHORT_PWD   "PWD"
#endif
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_A     "A"
#endif
//
// HELP_LEDSYSTEM
#if defined(COMMAND_SYSTEMENABLED)
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
#endif
//
// HELP_MOTORVNH2SP30
#define SHORT_MCP   "MCP"
#define SHORT_MCN   "MCN"
#define SHORT_MPA   "MPA"
#define SHORT_MPR   "MPR"
#define SHORT_GPT   "GPT"
#define SHORT_SPA   "SPA"
#define SHORT_GPA   "GPA"
#define SHORT_ACM   "ACM"
#define SHORT_AAM   "AAM"
#define SHORT_GCV   "GCV"
#define SHORT_SCV   "SCV"
//
// HELP_ENCODER
#define SHORT_GEP   "GEP"
#define SHORT_SEP   "SEP"
//
// HELP_LIMITSWITCH
#define SHORT_GLS   "GLS"
//
// HELP_SERIAL_COMMAND
#define SHORT_WLC   "WLC"
#define SHORT_RLC   "RLC"
//
// HELP_IC2DISPLAY
#if defined(I2CDISPLAY_ISPLUGGED)
#define SHORT_CLI   "CLI"
#define SHORT_STI   "STI"
#endif
//
// HELP_SDCOMMAND
#if defined(SDCARD_ISPLUGGED)
#define SHORT_OCF   "OCF"
#define SHORT_WCF   "WCF"
#define SHORT_CCF   "CCF"
#define SHORT_ECF   "ECF"
#define SHORT_ACF   "ACF"
#endif
//
#define MASK_ENDLINE " ###"

//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
#define TITLE_LINE            "--------------------------------------------------"
#define MASK_PROJECT          "- Project:   %-35s -"
#define MASK_SOFTWARE         "- Version:   %-35s -"
#define MASK_HARDWARE         "- Hardware:  %-35s -"
#define MASK_DATE             "- Date:      %-35s -"
#define MASK_TIME             "- Time:      %-35s -"
#define MASK_AUTHOR           "- Author:    %-35s -"
#define MASK_PORT             "- Port:      %-35s -"
#define MASK_PARAMETER        "- Parameter: %-35s -"
//
//-----------------------------------------------------------------------------------------
//
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_GPH                  " %-3s      : Get Program Header"
#define MASK_GSV                  " %-3s      : Get Software-Version"
#define MASK_GHV                  " %-3s      : Get Hardware-Version"
#define MASK_GPC                  " %-3s      : Get Process Count"
#define MASK_SPC                  " %-3s <pc> : Set Process Count"
#define MASK_GPP                  " %-3s      : Get Process Period{ms}"
#define MASK_SPP                  " %-3s <pp> : Set Process Period{ms}"
#define MASK_GPW                  " %-3s      : Get Process Width {us}"
#define MASK_SPW                  " %-3s <pw> : Set Process Width {us}"
#endif
#define MASK_RSS                  " %-3s      : Reset System"
#if defined(WATCHDOG_ISPLUGGED)
#define MASK_PWD                  " %-3s      : Pulse WatchDog"
#endif
#if defined(COMMAND_SYSTEMENABLED)
#define MASK_A                    " %-1s      : Abort Process Execution"
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s         : Get State LedSystem"
#define MASK_LSH                  " %-3s         : Switch LedSystem On"
#define MASK_LSL                  " %-3s         : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <p> <n> : Blink LedSystem with <p>eriod{ms} <n>times"
#endif
//
#define HELP_MOTORVNH2SP30        " Help (Motor VNH2SP30):"
#define MASK_MCP                  " %-3s <c>     : Move <c>hannel Positive"
#define MASK_MCN                  " %-3s <c>     : Move <c>hannel Negative"
#define MASK_MPA                  " %-3s <c> <p> : Move <c>hannel <p>osition Absolute"
#define MASK_MPR                  " %-3s <c> <p> : Move <c>hannel <p>osition Relative"
#define MASK_GPT                  " %-3s <c>     : Get <c>hannel Position Target"
#define MASK_SPA                  " %-3s <c> <p> : Set <c>hannel <p>osition Actual"
#define MASK_GPA                  " %-3s <c>     : Get <c>hannel Position Actual"
#define MASK_ACM                  " %-3s <c>     : Abort <c>hannel Motion"
#define MASK_AAM                  " %-3s         : Abort All Motion"
#define MASK_GCV                  " %-3s <c>     : Get <c>hannel Velocity [0..100%]"
#define MASK_SCV                  " %-3s <c> <v> : Set <c>hannel <v>elocity [0..100%]"
//
#define HELP_ENCODER              " Help (Encoder):"
#define MASK_GEP                  " %-3s     : Get Encoder Position"
#define MASK_SEP                  " %-3s <p> : Set Encoder <p>osition [stp]"
//
#define HELP_LIMITSWITCH          " Help (Limitswitch):"
#define MASK_GLS                  " %-3s : Get Limitswitches"
//
//#define HELP_SERIAL_COMMAND       " Help (Serial Command):"
//#define MASK_WLC                  " %-3s <l> : Write <l>ine"
//#define MASK_RLC                  " %-3s     : Read Line"
//
#if defined(I2CDISPLAY_ISPLUGGED)
#define HELP_I2CDISPLAY            " Help (I2CDisplay):"
#define MASK_CLI                  " %-3s             : Clear Display"
#define MASK_STI                  " %-3s <r> <c> <t> : Show <t>ext at <r>ow <c>ol"
#endif
//
#if defined(SDCARD_ISPLUGGED)
#define HELP_SDCOMMAND            " Help (SDCommand):"
#define MASK_OCF                  " %-3s <f>        : Open Command<f>ile for writing"
#define MASK_WCF                  " %-3s <c> <p> .. : Write <c>ommand with <p>arameter(s) to File"
#define MASK_CCF                  " %-3s            : Close Command<f>ile for writing"
#define MASK_ECF                  " %-3s <f>        : Execute Command<f>ile"
#define MASK_ACF                  " %-3s            : Abort Execution Commandfile"
#endif
//
// 
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version MEC%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_TEXTPARAMETERS 5
#define SIZE_TEXTPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Int16 FRxdBufferIndex = 0;
  Character FCommandText[SIZE_RXDBUFFER];
  PCharacter FPCommand;
  PCharacter FPParameters[COUNT_TEXTPARAMETERS];
  Int16 FParameterCount = 0;

  public:
  CCommand();
  ~CCommand();
  //
  inline PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  //
  inline PCharacter GetPCommand()
  {
    return FPCommand;
  }
  inline Byte GetParameterCount()
  {
    return FParameterCount;
  }
  inline PCharacter GetPParameters(UInt8 index)
  {
    return FPParameters[index];
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroCommandText();
  //
  //  Segment - Execution - Common
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteGetProcessCount(CSerial &serial);
  void ExecuteSetProcessCount(CSerial &serial);
  void ExecuteGetProcessPeriod(CSerial &serial);
  void ExecuteSetProcessPeriod(CSerial &serial);
  void ExecuteGetProcessWidth(CSerial &serial);
  void ExecuteSetProcessWidth(CSerial &serial);
  void ExecuteAbortAll(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - System
  void ExecuteResetSystem(CSerial &serial); 
#if defined(WATCHDOG_ISPLUGGED)
  void ExecutePulseWatchDog(CSerial &serial); 
#endif
  //
  //  Segment - Execution - MotorVNH2SP30
  void ExecuteMoveChannelPositive(CSerial &serial);
  void ExecuteMoveChannelNegative(CSerial &serial);
  void ExecuteMoveAbsolute(CSerial &serial);
  void ExecuteMoveRelative(CSerial &serial);
  void ExecuteGetStepCountTarget(CSerial &serial);
  void ExecuteGetStepCountActual(CSerial &serial);
  void ExecuteSetStepCountActual(CSerial &serial);
  void ExecuteAbortChannelMotion(CSerial &serial);
  void ExecuteAbortAllMotion(CSerial &serial);
  void ExecuteGetChannelVelocity(CSerial &serial);
  void ExecuteSetChannelVelocity(CSerial &serial);
  //
  //  Segment - Execution - Encoder
  void ExecuteGetEncoderStepCount(CSerial &serial);
  void ExecuteSetEncoderStepCount(CSerial &serial);
  //
  //  Segment - Execution - Limitswitch
  void ExecuteGetLimitSwitch(CSerial &serial);
  //
  //  Segment - Execution - SerialCommand
  void ExecuteWriteLineSerialCommand(CSerial &serial);
  void ExecuteReadLineSerialCommand(CSerial &serial);
  //
  //  Segment - Execution - I2CDisplay
#if defined(I2CDISPLAY_ISPLUGGED)
  void ExecuteClearScreenI2CDisplay(CSerial &serial); 
  void ExecuteShowTextI2CDisplay(CSerial &serial);
#endif
  //
  //  Segment - Execution - SDCommand
#if defined(SDCARD_ISPLUGGED)
  void ExecuteOpenCommandFile(CSerial &serial);
  void ExecuteWriteCommandFile(CSerial &serial);
  void ExecuteCloseCommandFile(CSerial &serial);
  void ExecuteExecuteCommandFile(CSerial &serial);
  void ExecuteAbortCommandFile(CSerial &serial);
#endif
  //  
  // Main
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
#if defined(SDCARD_ISPLUGGED) 
  Boolean DetectSDCardLine(CSDCard &sdcard);
#endif  
  Boolean AnalyseCommandText(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  void RiseEvent(CSerial &serial, String message);  
#if defined(SDCARD_ISPLUGGED)
  Boolean Handle(CSerial &serial, CSDCard &sdcard);
#else  
  Boolean Handle(CSerial &serial);
#endif
  Boolean Execute(CSerial &serial);
};
//
#endif // MECCommand_h
