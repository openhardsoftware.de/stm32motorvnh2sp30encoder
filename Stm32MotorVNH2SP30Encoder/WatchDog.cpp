//
//--------------------------------
//  Library WatchDog
//--------------------------------
//
#include "WatchDog.h"
//
CWatchDog::CWatchDog(int pin) 
  : CIOPin(pin, iopdOutput, false)
{
  FCounterPreset = 2;
  FPresetHigh = 1;
  FCounterActual = 0;
  CIOPin::Open();
  CIOPin::SetOff(); 
}

Boolean CWatchDog::Open(UInt32 counterpreset, UInt32 presethigh)
{
  CIOPin::Open();
  FCounterPreset = counterpreset;
  FPresetHigh = presethigh;
  FCounterActual = 0;
  CIOPin::Open();
  CIOPin::SetOff(); 
  return true;
}

Boolean CWatchDog::Close()
{
  CIOPin::Close();
  return true;
}

void CWatchDog::SetOn()
{
  CIOPin::SetOn();
}

void CWatchDog::SetOff()
{
  CIOPin::SetOff();  
}

void CWatchDog::Trigger()
{
  if (FCounterPreset < FCounterActual)
  {
    FCounterActual = 0;
    SetOn();
    return;
  }
  if (FCounterActual < FPresetHigh)
  {
    SetOn();
  }
  else
  {
    SetOff();
  }
  FCounterActual++;
}

void CWatchDog::ForceTrigger()
{
  FCounterActual = 0;
  SetOff();
  delay(50);
  SetOn();
  delay(100);
  SetOff();
  delay(50);
}


