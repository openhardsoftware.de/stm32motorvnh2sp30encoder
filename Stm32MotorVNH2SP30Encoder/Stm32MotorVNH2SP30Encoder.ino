#include "MECDefines.h"
#include "MECPinout.h"
#include "MECError.h"
#include "MECProcess.h"
#include "MECCommand.h"
#include "MECAutomation.h"
#include "Serial.h"
#include "Led.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(SDCARD_ISPLUGGED)
#include "SDCard.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#include "IOPin.h"
#include "MotorDriverVNH2SP30.h"
#include "MotorEncoder.h"
#include "JoystickAnalog.h"
#include "Key.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
// 
//------------------------------------------------------
// Segment - Global Variables 
//------------------------------------------------------
//
CError      MECError;
CProcess    MECProcess;
CCommand    MECCommand;
CAutomation MECAutomation;
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Led
//-----------------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//-----------------------------------------------------------
//
CSerial SerialCommand(SERIALCOMMAND);
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - SDCard
//-----------------------------------------------------------
//
#if defined(SDCARD_ISPLUGGED)
CSDCard SDCard(PIN_SPI_CS_SDCARD);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - I2CDisplay
//-----------------------------------------------------------
//
#if defined(I2CDISPLAY_ISPLUGGED)
CI2CDisplay I2CDisplay(I2CLCD_I2CADDRESS, I2CLCD_COLCOUNT, I2CLCD_ROWCOUNT);
#endif
//
//-----------------------------------------------------------
// Segment - Global Variables - Assignment - IOPin
//-----------------------------------------------------------
//
#if defined(WATCHDOG_ISPLUGGED)
CWatchDog WatchDog(PIN_23_A9_PWM_TOUCH);
#endif
//
//---------------------------------------------------------------
// Segment - Global Variables - Assignment - Motor / Encoder PX
//---------------------------------------------------------------
// 
CMotorDriverVNH2SP30 MotorSlitPX(PIN_MOTORSLITPX_INA, PIN_MOTORSLITPX_INB,
                                 PIN_MOTORSLITPX_ENAB, PIN_MOTORSLITPX_ENAB,
                                 PIN_MOTORSLITPX_PWM, PIN_MOTORSLITPX_CS);
CMotorEncoder  EncoderSlitPX(PIN_ENCODERSLITPX_ENA, PIN_ENCODERSLITPX_ENB,
                             ISREncoderSlitPXPulseA, ISREncoderSlitPXPulseB);
//
//---------------------------------------------------------------
// Segment - Global Variables - Assignment - Motor / Encoder DX
//---------------------------------------------------------------
// 
CMotorDriverVNH2SP30 MotorSlitDX(PIN_MOTORSLITDX_INA, PIN_MOTORSLITDX_INB,
                                 PIN_MOTORSLITDX_ENAB, PIN_MOTORSLITDX_ENAB,
                                 PIN_MOTORSLITDX_PWM, PIN_MOTORSLITDX_CS);
CMotorEncoder  EncoderSlitDX(PIN_ENCODERSLITDX_ENA, PIN_ENCODERSLITDX_ENB,
                             ISREncoderSlitDXPulseA, ISREncoderSlitDXPulseB);   
//
//---------------------------------------------------------------
// Segment - Global Variables - Assignment - Joystick
//---------------------------------------------------------------
// 
CJoystickAnalog JoystickSlitPXDX(PIN_JOYSTICK_PX, PIN_JOYSTICK_DX);
//
//---------------------------------------------------------------
// Segment - Global Variables - Assignment - Key
//---------------------------------------------------------------
// 
//CKey KeyUp(PIN_KEY_UP, KEY_INVERTED);
//CKey KeyLeft(PIN_KEY_LEFT, KEY_INVERTED);
//CKey KeyRight(PIN_KEY_RIGHT, KEY_INVERTED);
//CKey KeyDown(PIN_KEY_DOWN, KEY_INVERTED);
//
//------------------------------------------------------
// Segment - Forward-Declaration
//------------------------------------------------------
//
// ....
//
//------------------------------------------------------
// Segment - IRQ - Callback
//------------------------------------------------------
//
// IRQ - EncoderSlitPX
//
Int32 StepCountActual = 0;
Int32 StepCountPreset = 0;
long unsigned ErrorCount;

void ISREncoderSlitPXPulseA(void)
{
  LedSystem.SetOn();
  EncoderSlitPX.HandleInterrupt();
  LedSystem.SetOff();   
}

void ISREncoderSlitPXPulseB(void)
{  
  LedSystem.SetOn();
  EncoderSlitPX.HandleInterrupt();
  LedSystem.SetOff();
}
//
// IRQ - EncoderSlitDX
//
void ISREncoderSlitDXPulseA(void)
{
  LedSystem.SetOn();
  EncoderSlitDX.HandleInterrupt();
  LedSystem.SetOff();   
}

void ISREncoderSlitDXPulseB(void)
{  
  LedSystem.SetOn();
  EncoderSlitDX.HandleInterrupt();
  LedSystem.SetOff();
}
//
//------------------------------------------------------
// Segment - Setup
//------------------------------------------------------
//
void setup() 
{ //
  //----------------------------------------------------
  // Device - IOPin
  //----------------------------------------------------
#if defined(WATCHDOG_ISPLUGGED)
  WatchDog.Open(WATCHDOG_COUNTERPRESET, WATCHDOG_PRESETHIGH);
#endif
  //----------------------------------------------------
  // Device - Led - LedSystem
  //----------------------------------------------------
  LedSystem.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.SetOff();
    delay(60);
    LedSystem.SetOn();
    delay(30);
  }
  LedSystem.SetOff();
  // From here LedSystem(Pin13)-Conflict with SPI-SCK!!!
  //  
  //----------------------------------------------------
  // Device - Serial
  //----------------------------------------------------
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_ON);
  delay(300);
  // 
  //----------------------------------------------------
  // Device - SDCard
  //----------------------------------------------------
#if defined(SDCARD_ISPLUGGED)
  SDCard.Mount();
  SDCard.Unmount();
#endif  
  //
  //----------------------------------------------------
  // Device - I2CDisplay
  //----------------------------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  I2CDisplay.Open();
  I2CDisplay.ClearDisplay();
  I2CDisplay.SetBacklightOn();
#endif
  //
  //----------------------------------------------------
  // Device - Motor
  //----------------------------------------------------
  MotorSlitPX.Open();
  MotorSlitDX.Open();
  //!!!MotorSlitPX.SetPwmPercent(30);
  //!!!MotorSlitPX.SetPwmPercent(30);
  //
  //----------------------------------------------------
  // Device - Encoder
  //----------------------------------------------------
  EncoderSlitPX.Open();
  EncoderSlitPX.SetStepCount(1);                               
  EncoderSlitDX.Open();
  EncoderSlitDX.SetStepCount(1);                               
  //
  //----------------------------------------------------
  // Device - Joystick
  //----------------------------------------------------
  JoystickSlitPXDX.Open();
  //
  //----------------------------------------------------
  // Device - Key
  //----------------------------------------------------
//  KeyUp.Open();
//  KeyLeft.Open();
//  KeyRight.Open();
//  KeyDown.Open();
  //
  //####################################################
  //
  //----------------------------------------------------
  // Device - Command
  //----------------------------------------------------
  MECCommand.WriteProgramHeader(SerialCommand);
  MECCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //----------------------------------------------------
  // Device - Process 
  //----------------------------------------------------
  MECProcess.Open();
  MECProcess.SetState(spWelcome);
  //----------------------------------------------------
  // Device - Automation
  //----------------------------------------------------
  MECAutomation.Open();
}
//
//------------------------------------------------------
// Segment - Loop
//------------------------------------------------------
//
void loop() 
{ 
  MECError.Handle(SerialCommand);    
#if defined(SDCARD_ISPLUGGED)
  MECCommand.Handle(SerialCommand, SDCard);
#else // without SDCard-Command-Processing:
  MECCommand.Handle(SerialCommand);
#endif
  MECProcess.Handle(SerialCommand); 
  MECAutomation.Handle(SerialCommand); 
  //
  StepCountActual = EncoderSlitPX.GetStepCount();
  if (StepCountActual != StepCountPreset)
  {
    SERIALCOMMAND.print("MEC[");
    SERIALCOMMAND.print(StepCountPreset);
    SERIALCOMMAND.print("] SCA[");
    SERIALCOMMAND.print(StepCountActual);
    SERIALCOMMAND.println("]");
    StepCountPreset = StepCountActual;
  }
  ErrorCount = EncoderSlitPX.GetErrorCount();
  if (0 < ErrorCount)
  {
    SERIALCOMMAND.print("!EC[");
    SERIALCOMMAND.print(ErrorCount);
    SERIALCOMMAND.println("]");
  }
//  delay(1000);
}
