//
//--------------------------------
//  Library JoystickAnalog
//--------------------------------
//
#include "JoystickAnalog.h"
//
CJoystickAnalog::CJoystickAnalog(int pinh, int pinv)
{
  FPinH = pinh;
  FPinV = pinv;
  FState = sjaUndefined;
  FAdcValueMinimum = ADCVALUE_MINIMUM;
  FAdcValueMaximum = ADCVALUE_MAXIMUM;
  FAdcZeroInterval = ADCZERO_INTERVAL;
}

EStateJoystickAnalog CJoystickAnalog::GetState()
{
  return FState;
}

Boolean CJoystickAnalog::Open()
{
  pinMode(FPinH, INPUT);
  pinMode(FPinV, INPUT);
  FState = sjaOff;
  return true;
}

Boolean CJoystickAnalog::Close()
{
  pinMode(FPinH, INPUT);
  pinMode(FPinV, INPUT);
  FState = sjaUndefined;
  return true;
}
//
UInt16 CJoystickAnalog::ReadAdcValueH(void)
{
  FAdcValueH = analogRead(FPinH);
  return FAdcValueH;
}
UInt16 CJoystickAnalog::ReadAdcValueV(void)
{
  FAdcValueV = analogRead(FPinV);
  return FAdcValueV;
}

Int32 CJoystickAnalog::ReadStickValueH(void)
{
  FAdcValueH = analogRead(FPinH);
  UInt16 AV = FAdcValueH;
  Int32 DAL = (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2 - ADCZERO_INTERVAL;
  Int32 DAH = (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2 + ADCZERO_INTERVAL;
  if ((DAL <= AV) && (AV <= DAH))
  {
    AV = (1 + ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2;
  }
  return (Int16)(STICKVALUE_MINIMUM + ((STICKVALUE_MAXIMUM - STICKVALUE_MINIMUM) * (AV - ADCVALUE_MINIMUM)) / (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM));
}
Int32 CJoystickAnalog::ReadStickValueV(void)
{
  FAdcValueV = analogRead(FPinV);
  UInt16 AV = FAdcValueV;
  Int32 DAL = (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2 - ADCZERO_INTERVAL;
  Int32 DAH = (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2 + ADCZERO_INTERVAL;
  if ((DAL <= AV) && (AV <= DAH))
  {
    AV = (1 + ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM) / 2;
  }
  return (Int16)(STICKVALUE_MINIMUM + ((STICKVALUE_MAXIMUM - STICKVALUE_MINIMUM) * (AV - ADCVALUE_MINIMUM)) / (ADCVALUE_MAXIMUM - ADCVALUE_MINIMUM));
}
