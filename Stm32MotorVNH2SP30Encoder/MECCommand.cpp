//
#include "MECError.h"
#include "MECProcess.h"
#include "MECCommand.h"
#include "Led.h"
#if defined(WATCHDOG_ISPLUGGED)
#include "WatchDog.h"
#endif
#if defined(I2CDISPLAY_ISPLUGGED)
#include "I2CDisplay.h"
#endif
#include "IOPin.h"
#include "MotorEncoder.h"
#include "MotorDriverVNH2SP30.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CSerial  SerialUSBProgram;
extern CSerial  SerialPrinter;
extern CSerial  SerialNetwork;
extern CSerial  SerialCommand;
extern CError   MECError;
extern CProcess MECProcess;
extern CCommand MECCommand;
extern CLed LedSystem;
#if defined(I2CDISPLAY_ISPLUGGED)
extern CI2CDisplay I2CDisplay;
#endif
#if defined(WATCHDOG_ISPLUGGED)
extern CWatchDog WatchDog;
#endif
//
extern CMotorEncoder  EncoderSlitPX;
extern CMotorDriverVNH2SP30 MotorSlitPX;
//extern CMotorEncoder  EncoderSlitDX;
//extern CMotorDriverVNH2SP30 MotorSlitDX;
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
// Defines Reset-Function (identically HW-Reset):
void (*PResetSystem)(void) = 0;
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroCommandText()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FCommandText[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(MECCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(MECCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::RiseEvent(CSerial &serial, String message)
{
  sprintf(MECCommand.GetTxdBuffer(), MASK_EVENTAUTOMATION, PROMPT_EVENT, message.c_str());
  serial.WriteLine(MECCommand.GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroCommandText();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FCommandText, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)  
Boolean CCommand::DetectSDCardLine(CSDCard &sdcard)
{
  if (spIdle == MECProcess.GetState())
  {
    if (MECProcess.IsExecutingCommandFile())
    {
      if (sdcard.IsOpenForReading())
      {
        String Line = sdcard.ReadTextLine();
        if (0 < strlen(FCommandText))
        {
          strcpy(FCommandText, Line.c_str()); 
          return true;      
        }
        else
        { 
          MECProcess.StopExecutionCommandFile();
        }
      }
    }
  }
  return false;
}
#endif

Boolean CCommand::AnalyseCommandText(CSerial &serial)
{
  char *PTerminal = (char*)" \t\r\n";
  //char *PCommandText = FCommandText;
  FPCommand = strtok(FCommandText, PTerminal);
  if (FPCommand)
  {
    FParameterCount = 0;
    char *PParameter;
    while (0 != (PParameter = strtok(0, PTerminal)))
    {
      FPParameters[FParameterCount] = PParameter;
      FParameterCount++;
      if (COUNT_TEXTPARAMETERS < FParameterCount)
      {
        MECError.SetCode(ecToManyParameters);
        ZeroRxdBuffer();
        serial.WriteNewLine();
        serial.WritePrompt();
        return false;
      }
    }  
    ZeroRxdBuffer();
    serial.WriteNewLine();
    return true;
  }
  ZeroRxdBuffer();
  serial.WriteNewLine();
  serial.WritePrompt();    
  return false;
}
//
#if defined(SDCARD_ISPLUGGED)
Boolean CCommand::Handle(CSerial &serial, CSDCard &sdcard)
{
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);     
    }
  }
  if (DetectSDCardLine(sdcard))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);
    }
  }
  return false;
}
#else
Boolean CCommand::Handle(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    if (AnalyseCommandText(serial))
    {
      return Execute(serial);     
    }
  }
  return false;
}
#endif
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer(); serial.WriteLine(MASK_ENDLINE);
}
//
void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}
//
void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}
//
void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.Write(MASK_H, SHORT_H); serial.WriteLine();
#if defined(COMMAND_SYSTEMENABLED)
  serial.WriteAnswer(); serial.Write(MASK_GPH, SHORT_GPH); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GSV, SHORT_GSV); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GHV, SHORT_GHV); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPC, SHORT_GPC); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPC, SHORT_SPC); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPP, SHORT_GPP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPP, SHORT_SPP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPW, SHORT_GPW); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPW, SHORT_SPW); serial.WriteLine();
#endif
  serial.WriteAnswer(); serial.Write(MASK_RSS, SHORT_RSS); serial.WriteLine();
#if defined(WATCHDOG_ISPLUGGED)
  serial.WriteAnswer(); serial.Write(MASK_PWD, SHORT_PWD); serial.WriteLine();
#endif
#if defined(COMMAND_SYSTEMENABLED)
  serial.WriteAnswer(); serial.Write(MASK_A, SHORT_A); serial.WriteLine();
#endif
  //
#if defined(COMMAND_SYSTEMENABLED)
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_LSH, SHORT_LSH); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_LSL, SHORT_LSL); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_BLS, SHORT_BLS); serial.WriteLine();
#endif
  // MotorVNH2SP30
  serial.WriteAnswer(); serial.WriteLine(HELP_MOTORVNH2SP30);
  serial.WriteAnswer(); serial.Write(MASK_MCP, SHORT_MCP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MCN, SHORT_MCN); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MPA, SHORT_MPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_MPR, SHORT_MPR); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPT, SHORT_GPT); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SPA, SHORT_SPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GPA, SHORT_GPA); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ACM, SHORT_ACM); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_AAM, SHORT_AAM); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_GCV, SHORT_GCV); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SCV, SHORT_SCV); serial.WriteLine();
  // Encoder
  serial.WriteAnswer(); serial.WriteLine(HELP_ENCODER);
  serial.WriteAnswer(); serial.Write(MASK_GEP, SHORT_GEP); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_SEP, SHORT_SEP); serial.WriteLine();
  // Limitswitch
  serial.WriteAnswer(); serial.WriteLine(HELP_LIMITSWITCH);
  serial.WriteAnswer(); serial.Write(MASK_GLS, SHORT_GLS); serial.WriteLine();
  //
//  serial.WriteAnswer(); serial.WriteLine(HELP_SERIAL_COMMAND); 
//  serial.WriteAnswer(); serial.Write(MASK_WLC, SHORT_WLC); serial.WriteLine();
//  serial.WriteAnswer(); serial.Write(MASK_RLC, SHORT_RLC); serial.WriteLine();
  //
#if defined(I2CDISPLAY_ISPLUGGED)
  serial.WriteAnswer(); serial.WriteLine(HELP_I2CDISPLAY);
  serial.WriteAnswer(); serial.Write(MASK_CLI, SHORT_CLI); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_STI, SHORT_STI); serial.WriteLine();
#endif
  //
#if defined(SDCARD_ISPLUGGED)  
  serial.WriteAnswer(); serial.WriteLine(HELP_SDCOMMAND);
  serial.WriteAnswer(); serial.Write(MASK_OCF, SHORT_OCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_WCF, SHORT_WCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_CCF, SHORT_CCF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ECF, SHORT_ECF); serial.WriteLine();
  serial.WriteAnswer(); serial.Write(MASK_ACF, SHORT_ACF); serial.WriteLine();
#endif
  //
  serial.WriteAnswer(); serial.WriteLine(MASK_ENDLINE);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), 0);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), 0);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetProcessCount(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetProcessCount);
    // Analyse parameters: -
    // Execute:
    UInt32 PC = MECProcess.GetProcessCount();    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetProcessCount);
    // Analyse parameters:
    UInt32 PC = atol(GetPParameters(0));
    // Execute:
    MECProcess.SetProcessCount(PC);
    PC = MECProcess.GetProcessCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetProcessPeriod);
    // Analyse parameters: -
    // Execute:
    UInt32 PP = MECProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetProcessPeriod);
    // Analyse parameters:
    UInt32 PP = atol(GetPParameters(0));
    // Execute:
    MECProcess.SetProcessPeriodms(PP);
    PP = MECProcess.GetProcessPeriodms();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetProcessWidth(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetProcessWidth);
    // Analyse parameters: -
    // Execute:
    UInt32 PW = MECProcess.GetProcessWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteSetProcessWidth(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetProcessWidth);
    // Analyse parameters:
    UInt32 PW = atol(GetPParameters(0));
    // Execute:
    MECProcess.SetProcessWidthus(PW);
    PW = MECProcess.GetProcessWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
void CCommand::ExecuteResetSystem(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spResetSystem);
    // Analyse parameters ( RSS - ):
    // Execute:
    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
    delay(2000);
    //
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Reset System !!!
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#if (defined(PROCESSOR_NANOR3)||defined(PROCESSOR_UNOR3)||defined(PROCESSOR_MEGA2560))
    PResetSystem();
#elif defined(PROCESSOR_DUEM3)
#elif defined(PROCESSOR_STM32F103C8)
#elif (defined(PROCESSOR_TEENSY32)||defined(PROCESSOR_TEENSY36))
    // problem because of reprogramming !!! _reboot_Teensyduino_();
    // undefined _restart_Teensyduino_();
    // undefined init_pins();
    // the only possibility for this time:
    // OK (no reset!) _init_Teensyduino_internal_();
    // TOP:!!!
#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);
    CPU_RESTART
    //   
#endif
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    delay(2000);
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

#if defined(WATCHDOG_ISPLUGGED)
void CCommand::ExecutePulseWatchDog(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spPulseWatchDog);
    // Analyse parameters ( PWD - ):
    // Execute:
    WatchDog.ForceTrigger();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif

//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteAbortAll(CSerial &serial)
{ 
  MECProcess.SetProcessStep(0);
  MECProcess.SetProcessCount(0);
  MECProcess.SetState(spIdle);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.SetOn();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.SetOff();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
#if defined(COMMAND_SYSTEMENABLED)
void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spBlinkLedSystem);
    // Analyse parameters:
    UInt32 Period = atol(GetPParameters(0)); // [us]!!!!!!!!
    UInt32 Count = atol(GetPParameters(1));  // [1] 
    // Execute:
    MECProcess.SetProcessPeriodms(Period);
    MECProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPCommand(), Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
//#########################################################
//  Segment - Execution - MotorVNH2SP30
//#########################################################
//
void CCommand::ExecuteMoveChannelPositive(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spMoveChannelPositive);
    // Analyse parameters ( MCP <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.StartPositive();
        break;
//      case 1:
//        MotorSlitDX.StartPositive();
//        break;
      default:
        MECError.SetCode(ecCommandInvalidParameter);
        return;      
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMoveChannelNegative(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spMoveChannelNegative);
    // Analyse parameters ( MCN <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.StartNegative();
        break;
//      case 1:
//        MotorSlitDX.StartNegative();
//        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMoveAbsolute(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spMoveAbsolute);
    // Analyse parameters ( MPA <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.MoveAbsolute(PA);
        break;
//      case 1:
//        MotorSlitDX.MoveAbsolute(PA);
//        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
                            PROMPT_RESPONSE, GetPCommand(),
                            Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteMoveRelative(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spMoveRelative);
    // Analyse parameters ( MPR <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PR = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.MoveRelative(PR);
        break;
//      case 1:
//        MotorSlitDX.MoveRelative(PR);
//        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PR);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteGetStepCountTarget(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetStepCountTarget);
    // Analyse parameters ( GPT <c> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PT = 0;
    // Execute:
    switch (Channel)
    {
      case 0:
        PT = MotorSlitPX.GetPositionTarget();
        break;
//      case 1:
//        PT = MotorSlitDX.GetPositionTarget();
//        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PT);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteGetStepCountActual(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetStepCountActual);
    // Analyse parameters ( GPA <c> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = 0;
    // Execute:
    switch (Channel)
    {
      case 0:
        PA = MotorSlitPX.GetPositionActual();
        break;
//      case 1:
//        PA = MotorSlitDX.GetPositionActual();
//        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteSetStepCountActual(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetStepCountActual);
    // Analyse parameters ( SPA <c> <p> ):
    Byte Channel = atoi(GetPParameters(0));
    Int32 PA = atoi(GetPParameters(1));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.SetPositionActual(PA);
        break;
//      case 1:
//        MotorSlitDX.SetPositionActual(PA);
//        break;
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %i", 
            PROMPT_RESPONSE, GetPCommand(), Channel, PA);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteAbortChannelMotion(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spAbortChannelMotion);
    // Analyse parameters ( ACM <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    switch (Channel)
    {
      case 0:
        MotorSlitPX.StopMotion();
        break;
//      case 1:
//        MotorSlitDX.StopMotion();
//        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteAbortAllMotion(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spAbortAllMotion);
    // Analyse parameters ( AAM <-> ):
    // Execute:
    MotorSlitPX.StopMotion();
//    MotorSlitDX.StopMotion();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteGetChannelVelocity(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetChannelVelocity);
    // Analyse parameters ( GCV <a/x/y> ):
    String SI = strupr(GetPParameters(0));  // [1]
    int VP = 0;
    // Execute:
    switch (SI[0])
    {
      case 'x': case 'X':
        VP = MotorSlitPX.GetVelocityPercent();
        break;
//      case 'y': case 'Y':
//        VP = MotorSlitDX.GetVelocityPercent();
//        break;
      default:
        SI = "X";
        VP = MotorSlitPX.GetVelocityPercent();
        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), SI.c_str(), VP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}

void CCommand::ExecuteSetChannelVelocity(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetChannelVelocity);
    // Analyse parameters ( SCV <a/x/y> <v> ):
    String SI = strupr(GetPParameters(0));  // [1]
    int VP = atoi(GetPParameters(1));  // [%] 
    // Execute:
    switch (SI[0])
    {
      case 'x': case 'X':
        MotorSlitPX.SetVelocityPercent(VP);
        break;
//      case 'y': case 'Y':
//        MotorSlitDX.SetVelocityPercent(VP);
//        break;
      default:
        SI = "A";
        MotorSlitPX.SetVelocityPercent(VP);
//        MotorSlitDX.SetVelocityPercent(VP);
        break;
    }
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %s %i", 
            PROMPT_RESPONSE, GetPCommand(), SI.c_str(), VP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
//
//void CCommand::ExecuteMotorStop(CSerial &serial)
//{
//  if (spIdle == MECProcess.GetState())
//  {
//    MECProcess.SetState(spMotorStop);
//    // Analyse parameters ( MST <a/x/y> ):
//    String SIndex = strupr(GetPParameters(0));  // [1]
//    if (0 == SIndex.c_str())
//    {
//      SIndex = "A";
//    }
//    // Execute:
//    switch (SIndex[0])
//    {
//      case 'x': case 'X':
//        MotorSlitPX.StopMotion();
//        break;
//      case 'y': case 'Y':
//        MotorSlitDX.StopMotion();
//        break;
//      default:
//        MotorSlitPX.StopMotion();
//        MotorSlitDX.StopMotion();
//    }
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), SIndex.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    MECError.SetCode(ecCommandTimingFailure);
//  }  
//}
//
//void CCommand::ExecuteRotateClockwise(CSerial &serial)
//{
//  if (spIdle == MECProcess.GetState())
//  {
//    MECProcess.SetState(spRotateClockwise);
//    // Analyse parameters ( RCW <a/x/y> ):
//    String SIndex = strupr(GetPParameters(0));  // [1]
//    if (0 == SIndex.c_str())
//    {
//      SIndex = "A";
//    }
//    // Execute:
//    switch (SIndex[0])
//    {
//      case 'x': case 'X':
//        MotorSlitPX.StartNegative();
//        break;
//      case 'y': case 'Y':
//        MotorSlitDX.StartNegative();
//        break;
//      default:
//        MotorSlitPX.StartNegative();
//        MotorSlitDX.StartNegative();
//    }
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), SIndex.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    MECError.SetCode(ecCommandTimingFailure);
//  }  
//}
//
//void CCommand::ExecuteRotateCounterClockwise(CSerial &serial)
//{
//  if (spIdle == MECProcess.GetState())
//  {
//    MECProcess.SetState(spRotateCounterClockwise);
//    // Analyse parameters ( RCC <a/x/y> ):
//    String SIndex = strupr(GetPParameters(0));  // [1]
//    if (0 == SIndex.c_str())
//    {
//      SIndex = "A";
//    }
//    // Execute:
//    switch (SIndex[0])
//    {
//      case 'x': case 'X':
//        MotorSlitPX.StartPositive();
//        break;
//      case 'y': case 'Y':
//        MotorSlitDX.StartPositive();
//        break;
//      default:
//        MotorSlitPX.StartPositive();
//        MotorSlitDX.StartPositive();
//    }
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), SIndex.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    MECError.SetCode(ecCommandTimingFailure);
//  }  
//}
//
//
//#########################################################
//  Segment - Execution - Encoder
//#########################################################
//
void CCommand::ExecuteGetEncoderStepCount(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetEncoderStepCount);
    // Analyse parameters:
    String SIndex = strupr(GetPParameters(0));  // [1]
    if (0 == SIndex.c_str())
    {
      SIndex = "X";
    }
    // Execute:
    Int32 StepCount = -9999;
    switch (SIndex[0])
    {
      case 'x': case 'X':
        StepCount = EncoderSlitPX.GetStepCount();
        break;
//      case 'y': case 'Y':
//        StepCount = EncoderSlitDX.GetStepCount();
//        break;
      default:
        SIndex = "X";
        StepCount = EncoderSlitPX.GetStepCount();
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %s %li", 
            PROMPT_RESPONSE, GetPCommand(), SIndex.c_str(), StepCount);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }  
}

void CCommand::ExecuteSetEncoderStepCount(CSerial &serial)
{
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spSetEncoderStepCount);
    // Analyse parameters:
    String SIndex = strupr(GetPParameters(0));  // [1]
    Int32 StepCount = atol(GetPParameters(1));
    if (0 == SIndex.c_str())
    {
      SIndex = "A";
    }
    // Execute:
    switch (SIndex[0])
    {
      case 'x': case 'X':
        EncoderSlitPX.SetStepCount(StepCount);
        break;
//      case 'y': case 'Y':
//        EncoderSlitDX.SetStepCount(StepCount);
//        break;
      default:
        EncoderSlitPX.SetStepCount(StepCount);
//        EncoderSlitDX.SetStepCount(StepCount);
    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %s %li", 
            PROMPT_RESPONSE, GetPCommand(), SIndex.c_str(), StepCount);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }  
}
//
//
//#########################################################
//  Segment - Execution - LimitSwitch
//#########################################################
//
void CCommand::ExecuteGetLimitSwitch(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spGetLimitswitch);
    // Analyse parameters ( GLW <c> ):
    Byte Channel = atoi(GetPParameters(0));
    // Execute:
    Byte State = 0xFF;
//    if (0 < (0x01 & Channel))
//    {
//      Channel = 0x01;
//      State = MotorAxisUX.GetLimitSwitch();
//    } else
//    if (0 < (0x02 & Channel))
//    {
//      Channel = 0x02;
//      State = MotorAxisUY.GetLimitSwitch();
//    } else
//    if (0 < (0x04 & Channel))
//    {
//      Channel = 0x04;
//      State = MotorAxisVX.GetLimitSwitch();
//    } else
//    if (0 < (0x08 & Channel))
//    {
//      Channel = 0x08;
//      State = MotorAxisVY.GetLimitSwitch();
//    } 
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %u %u", 
            PROMPT_RESPONSE, GetPCommand(), Channel, State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}



////
////#########################################################
////  Segment - Execution - Serial - Command
////#########################################################
////
//void CCommand::ExecuteWriteLineSerialCommand(CSerial &serial)
//{ 
//  if (spIdle == MECProcess.GetState())
//  {
//    MECProcess.SetState(spWriteLineSerialCommand);
//    // Analyse parameters ( WLC <l> ):
//    String Line = GetPParameters(0);
//    // Execute:
//    SerialCommand.WriteLine(Line);
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), Line.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    MECError.SetCode(ecCommandTimingFailure);
//  }
//}
//
//void CCommand::ExecuteReadLineSerialCommand(CSerial &serial)
//{ 
//  if (spIdle == MECProcess.GetState())
//  {
//    MECProcess.SetState(spReadLineSerialCommand);
//    // Analyse parameters ( RLC - ):
//    // Execute:
//    String Line = SerialCommand.ReadLine();
//    // Response:
//    sprintf(GetTxdBuffer(), "%s %s %s", 
//            PROMPT_RESPONSE, GetPCommand(), Line.c_str());
//    serial.WriteLine(GetTxdBuffer());
//    serial.WritePrompt();
//  }
//  else
//  {
//    MECError.SetCode(ecCommandTimingFailure);
//  }
//}
//
//#########################################################
//  Segment - Execution - I2CDisplay
//#########################################################
//
#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteClearScreenI2CDisplay(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spClearScreenI2CDisplay);
    // Analyse parameters ( CLI ):
    // Execute:
    I2CDisplay.ClearDisplay();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif

#if defined(I2CDISPLAY_ISPLUGGED)
void CCommand::ExecuteShowTextI2CDisplay(CSerial &serial)
{ 
  if (spIdle == MECProcess.GetState())
  {
    MECProcess.SetState(spShowTextI2CDisplay);
    // Analyse parameters ( STI <c> <r> <t> ):
    Byte R = atol(GetPParameters(0));
    Byte C = atol(GetPParameters(1));
    String T = GetPParameters(2);
    // Execute:
    I2CDisplay.SetCursorPosition(R, C);    
    I2CDisplay.WriteText(T);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i %i %s", PROMPT_RESPONSE, GetPCommand(), R, C, T.c_str());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    MECError.SetCode(ecCommandTimingFailure);
  }
}
#endif
//
//#########################################################
//  Segment - Execution - SDCommand
//#########################################################
//
#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteOpenCommandFile(CSerial &serial)
{
  MECProcess.SetState(spOpenCommandFile);
  // Analyse parameters ( OCF <f> ):
  MECProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %s", 
          PROMPT_RESPONSE, GetPCommand(), GetPParameters(0));
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteWriteCommandFile(CSerial &serial)
{
  MECProcess.SetState(spWriteCommandFile);
  // Analyse parameters ( WCF <c> <p> ):
  // Execution: -
  // Response:
  serial.Write(PROMPT_RESPONSE);
  serial.Write(' ');
  serial.Write(GetPCommand());
  serial.Write(' ');
  int PC = GetParameterCount();    
  for (int II = 0; II < PC; II++)
  {
    serial.Write(GetPParameters(II));
    serial.Write(' ');
  }
  serial.WriteLine("");
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteCloseCommandFile(CSerial &serial)
{
  MECProcess.SetState(spCloseCommandFile);
  // Analyse parameters ( CCF - ):
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteExecuteCommandFile(CSerial &serial)
{ // ecf a.cmd
  MECProcess.SetState(spExecuteCommandFile);
  // Analyse parameters ( ECF <f> ):
  String CF = GetPParameters(0);
  MECProcess.SetFileName(GetPParameters(0));
  // Execution: -
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", 
          PROMPT_RESPONSE, GetPCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif

#if defined(SDCARD_ISPLUGGED)
void CCommand::ExecuteAbortCommandFile(CSerial &serial)
{
  MECProcess.SetState(spAbortCommandFile);
  // Analyse parameters ( ACF - ):
  // Execution:
//  Boolean Result = SDCard.CloseReadFile();
//  if (Result)
//  {
//    //... abort execution...
//  }
//  // Response:
//  sprintf(GetTxdBuffer(), "%s %s %u", 
//          PROMPT_RESPONSE, GetPCommand(), Result);
//  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
#endif
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  //  Common
  if (!strcmp(SHORT_H, GetPCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
#if defined(COMMAND_SYSTEMENABLED)  
  if (!strcmp(SHORT_GPH, GetPCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPC, GetPCommand()))
  {
    ExecuteGetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPP, GetPCommand()))
  {
    ExecuteGetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPW, GetPCommand()))
  {
    ExecuteGetProcessWidth(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPW, GetPCommand()))
  {
    ExecuteSetProcessWidth(serial);
    return true;
  } else 
#endif
  if (!strcmp(SHORT_RSS, GetPCommand()))
  {
    ExecuteResetSystem(serial);
    return true;
  } else
#if defined(WATCHDOG_ISPLUGGED)
  if (!strcmp(SHORT_PWD, GetPCommand()))
  {
    ExecutePulseWatchDog(serial);
    return true;
  } else
#endif
#if defined(COMMAND_SYSTEMENABLED)  
  if (!strcmp(SHORT_A, GetPCommand()))
  {
    ExecuteAbortAll(serial);
    return true;
  } else 
#endif  
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
#if defined(COMMAND_SYSTEMENABLED)
if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
#endif  
  // ----------------------------------
  // MotorVNH2SP30
  // ----------------------------------
  if (!strcmp(SHORT_MCP, GetPCommand()))
  {
    ExecuteMoveChannelPositive(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MCN, GetPCommand()))
  {
    ExecuteMoveChannelNegative(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MPA, GetPCommand()))
  {
    ExecuteMoveAbsolute(serial);
    return true;
  } else   
  if (!strcmp(SHORT_MPR, GetPCommand()))
  {
    ExecuteMoveRelative(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPT, GetPCommand()))
  {
    ExecuteGetStepCountTarget(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPA, GetPCommand()))
  {
    ExecuteSetStepCountActual(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPA, GetPCommand()))
  {
    ExecuteGetStepCountActual(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ACM, GetPCommand()))
  {
    ExecuteAbortChannelMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_AAM, GetPCommand()))
  {
    ExecuteAbortAllMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GCV, GetPCommand()))
  {
    ExecuteGetChannelVelocity(serial);
    return true;
  } else
  if (!strcmp(SHORT_SCV, GetPCommand()))
  {
    ExecuteSetChannelVelocity(serial);
    return true;
  } else
  // ----------------------------------
  // Encoder
  // ----------------------------------
  if (!strcmp(SHORT_GEP, GetPCommand()))
  {
    ExecuteGetEncoderStepCount(serial);
    return true;
  } else
  if (!strcmp(SHORT_SEP, GetPCommand()))
  {
    ExecuteSetEncoderStepCount(serial);
    return true;
  } else
  // ----------------------------------
  // LimitSwitch
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPCommand()))
  {
    ExecuteGetLimitSwitch(serial);
    return true;
  } else   
//  // ----------------------------------
//  // Serial Command
//  // ---------------------------------- 
//  if (!strcmp(SHORT_WLC, GetPCommand()))
//  {
//    ExecuteWriteLineSerialCommand(serial);
//    return true;
//  } else   
//  if (!strcmp(SHORT_RLC, GetPCommand()))
//  {
//    ExecuteReadLineSerialCommand(serial);
//    return true;
//  } else   
  // ----------------------------------
  // I2CDisplay
  // ----------------------------------
#if defined(I2CDISPLAY_ISPLUGGED)
  if (!strcmp(SHORT_CLI, GetPCommand()))
  {
    ExecuteClearScreenI2CDisplay(serial);
    return true;
  } else   
  if (!strcmp(SHORT_STI, GetPCommand()))
  {
    ExecuteShowTextI2CDisplay(serial);
    return true;
  } else  
#endif
  // ----------------------------------
  // SDCommand
  // ----------------------------------
#if defined(SDCARD_ISPLUGGED)
  if (!strcmp(SHORT_OCF, GetPCommand()))
  {
    ExecuteOpenCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_WCF, GetPCommand()))
  {
    ExecuteWriteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_CCF, GetPCommand()))
  {
    ExecuteCloseCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ECF, GetPCommand()))
  {
    ExecuteExecuteCommandFile(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ACF, GetPCommand()))
  {
    ExecuteAbortCommandFile(serial);
    return true;
  } else   
#endif
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    MECError.SetCode(ecInvalidCommand);
  }
  return false;  
}
