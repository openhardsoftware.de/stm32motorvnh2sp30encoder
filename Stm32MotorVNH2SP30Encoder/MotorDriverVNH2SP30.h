//
//--------------------------------
//  Library MotorDriverVNH2SP30
//--------------------------------
//
#ifndef MotorDriverVNH2SP30_h
#define MotorDriverVNH2SP30_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const int INIT_VELOCITYPWMPERCENT = 50; 
const int ZERO_VELOCITYPERCENT = 0;
//
const Byte PWM_MAXIMUM = 0xFE;
const Byte PWM_MINIMUM = 0x01;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateMotorDriverVNH2SP30
{
  smdUndefined = -1,
  smdIdle = 0,
  smdPositive = 1,
  smdNegative = 2
};
//
//--------------------------------
//  Section - CMotorDriverVNH2SP30
//--------------------------------
//
class CMotorDriverVNH2SP30
{
  private:
  int FPinPWM;
  int FPinINA;
  int FPinINB;
  int FPinENA;
  int FPinENB;
  int FPinCS;
  int FVelocityPercent;
  EStateMotorDriverVNH2SP30 FState;
  Int32 FPositionTarget, FPositionActual;
  //
  void SetState(EStateMotorDriverVNH2SP30 state);
  //
  public:
  CMotorDriverVNH2SP30(int pinina, int pininb, 
                       int pinena, int pinenb, 
                       int pinpwm, int pincs);
  //
  EStateMotorDriverVNH2SP30 GetState();
  //
  Byte VelocityPercentToPwm(int velocitypercent);
  int PwmToVelocityPercent(Byte pwm);
  // 
  Boolean Open();
  Boolean Close();
  //
  Int32 GetPositionTarget();
  Int32 GetPositionActual();
  void SetPositionActual(Int32 positionactual);
  int GetVelocityPercent(void); // [%]
  void SetVelocityPercent(int velocitypercent); // [%]
  //
  void StartPositive();
  void StartNegative();
  void MoveAbsolute(Int32 positionabsolute);
  void MoveRelative(Int32 positionrelative);
  void StopMotion();
  void BreakMotion(); 
};
//
#endif
