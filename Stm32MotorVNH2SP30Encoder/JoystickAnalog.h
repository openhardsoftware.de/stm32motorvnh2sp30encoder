//
//--------------------------------
//  Library JoystickAnalog
//--------------------------------
//
#ifndef JoystickAnalog_h
#define JoystickAnalog_h
//
#include "Arduino.h"
#include "MECDefines.h"
#include "MECPinout.h"
//
//--------------------------------
//  Section - Constant
//--------------------------------
//
const UInt16 ADCVALUE_MINIMUM = 0;
const UInt16 ADCVALUE_MAXIMUM = 4095;
const UInt16 ADCZERO_INTERVAL = 1;//80;
//
const Int16 STICKVALUE_MINIMUM = -100;
const Int16 STICKVALUE_MAXIMUM = +100;
//
//--------------------------------
//  Section - Type
//--------------------------------
//
enum EStateJoystickAnalog
{
  sjaUndefined = -1,
  sjaOff = 0,
  sjaOn = 1
};
//
class CJoystickAnalog
{
  private:
  int FPinH;
  int FPinV;
  EStateJoystickAnalog FState;
  UInt16 FAdcValueH;
  UInt16 FAdcValueV;
  UInt16 FAdcValueMinimum, FAdcValueMaximum, FAdcZeroInterval;
  //  
  public:
  CJoystickAnalog(int pinh, int pinv);
  //
  EStateJoystickAnalog GetState();
  //
  Boolean Open();
  Boolean Close();
  //
  UInt16 ReadAdcValueH(void);
  UInt16 ReadAdcValueV(void);
  Int32 ReadStickValueH(void);
  Int32 ReadStickValueV(void);
};
//
#endif
