# Stm32MotorVNH2Sp30Encoder

Stm32F103C8 - VNH2SP30 - RotationEncoder

### ToDo
O int PwmToVelocityPercent(Byte pwm) mit Leben füllen.
O Interrupts ABEncoder auswerten $\rightarrow$ Change Position, Check Motion.
O Encoder mit vierfacher Genauigkeit betreiben (IRQ A/B alternate).
O Anzeige I2CDisplay mit PositionActual und PositionTarget entsprechend:
  <pre><code>  PX +1234567 +1234567
  DX -1234567 -1234567
  PY +1234567 +1234567
  DY -1234567 -1234567  </code></pre>
1908271525 - Neue Commands
  O Neuer Command: GetPositionTarget(void)
  O Neuer Command: Int32 GetPositionActual(void)
  O Neuer Command: void SetPositionActual(Int32 positionactual)
  O Neuer Command: MoveAbsolute(Int32 positionabsolute)
  O Neuer Command: MoveRelative(Int32 positionrelative)
  O Neuer Command: void StopMotion(void)
  O Neuer Command: void BreakMotion(void)

### Development

#### 1910071512 - Stm32MotorEncoderController
/ Stm32MotorEncoderController als Projektoberbegriff für
  * zwei Einheiten Stm32MotorVNH2SP30Encoder [Stm32MVE] und
  * eine Einheit Esp32MotorControllerDispatcher [Esp32MCD].

/ Demoschaltung auf Testbrett mit vier 12V DC-Getriebemotoren und ABEncodern mit 11spr (**2x** "1910011719Stm32MotorVNH2SpEncoder.pdf" [Stm32MVE]) und 1x Esp32MCD.

#### 1910071453 - Stm32MotorVNH2SP30Encoder ####
X KEINE Neubenennung des Projekts Stm32MotorVNH2SP30Encoder -> 1910071444_Stm32MotorEncoderController.
O Git-Weiterführung des Projekts Stm32MotorVNH2SP30Encoder.
X Kommentieren der vier Keys UP, DOWN, LEFT und RIGHT.

#### Version "_Stm32MotorVNH2SP30Encoder _"
  * 1909021048 - ABEncoder-IRQ:  
    * Überprüfung der AB-IRQ-Pins,
    * ...
  * 1908291725 - Versuch Integration ABEncoder-IRQs (Stm32DualSlitController)
  $\dag$ Problem: nur negative Richtung wird registriert, Fehler in Level-Detection?

#### Version "1908271459_Stm32MotorVNH2SP30Encoder_01V03"
  * 1908271148 - Kopieren / Ersetzen der Stm32MotorVNH2SP30Encoder-"MotorVNH2SP30.cpp/.h"
   durch Stm32DualSlitController - "MotorDriverVNH2SP30.cpp/.h",
  * Testbetrieb ok, IRQ-AB-Encoder scheint zu funktionieren!!! (Debug-Ausgabe erfolgt!),
  * 1908271355 - Methoden umbenannt auf "VelocityPercent" mit $v_{percent} \in [0\% .. 100\%] \in \mathbb{N_0}$, entsprechend PWM.

#### Zusammenführung
1908261734 - Basis Version: **1908261149_Stm32MotorVNH2SP30Encoder_Preset**
  * Joystick kontrolliert Betrag und Richtung der Geschwindigkeit.

1908261734 - Preset Version: **1901301607_Stm32DualSlitController_IrqABDecoder**
  * IRQ-Funktionen A/B für VierQuadrantenDecoder

### AddOns

### Delivery
